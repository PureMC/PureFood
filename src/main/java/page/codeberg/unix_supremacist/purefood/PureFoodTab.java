package page.codeberg.unix_supremacist.purefood;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class PureFoodTab extends CreativeTabs {
    public PureFoodTab() {
        super(Tags.MODID);
    }

    @Override
    @SideOnly(Side.CLIENT)
    public Item getTabIconItem() {
        return GameRegistry.findItem(Tags.MODID, "metaItem");
    }

    @Override
    @SideOnly(Side.CLIENT)
    public String getTabLabel() {
        return Tags.MODID;
    }

    @Override
    public boolean hasSearchBar(){
        return true;
    }
}