package page.codeberg.unix_supremacist.purefood;

// Use this class for Strings only. Do not import any classes here. It will lead to issues with Mixins if in use!

import cpw.mods.fml.common.Loader;

public class Tags {
    public static final String MODID = "GRADLETOKEN_MODID";
    public static final String MODNAME = "GRADLETOKEN_MODNAME";
    public static final String VERSION = "GRADLETOKEN_VERSION";
    public static final String GROUPNAME = "GRADLETOKEN_GROUPNAME";
    public static boolean enviromine = Loader.isModLoaded("enviromine");
}