package page.codeberg.unix_supremacist.purefood;

import page.codeberg.unix_supremacist.purefood.config.Config;
import page.codeberg.unix_supremacist.purefood.config.DynamicConfigs;
import page.codeberg.unix_supremacist.purefood.item.ItemIterator;
import page.codeberg.unix_supremacist.purefood.recipe.Oredict;
import page.codeberg.unix_supremacist.purefood.recipe.Recipe;
import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.event.*;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraftforge.common.MinecraftForge;

public class CommonProxy {
    EventMangler EVENT_HANDLER = new EventMangler();
    public static CreativeTabs pftab = new PureFoodTab();


    // preInit "Run before anything else. Read your config, create blocks, items,
    // etc, and register them with the GameRegistry."
    public void preInit(FMLPreInitializationEvent event) 	{
        Config.syncronizeConfiguration(event.getSuggestedConfigurationFile());

        ItemIterator.load();
        Oredict.load();
        Recipe.load();
        MinecraftForge.EVENT_BUS.register(EVENT_HANDLER);
        FMLCommonHandler.instance().bus().register(EVENT_HANDLER);

        DynamicConfigs.commitConfigs();
    }

    // load "Do your mod setup. Build whatever data structures you care about. Register recipes."
    public void init(FMLInitializationEvent event) {


    }

    // postInit "Handle interaction with other mods, complete your setup based on this."
    public void postInit(FMLPostInitializationEvent event) {

    }

    public void serverAboutToStart(FMLServerAboutToStartEvent event) {

    }

    // register server commands in this event handler
    public void serverStarting(FMLServerStartingEvent event) {

    }

    public void serverStarted(FMLServerStartedEvent event) {

    }

    public void serverStopping(FMLServerStoppingEvent event) {

    }

    public void serverStopped(FMLServerStoppedEvent event) {

    }
}
