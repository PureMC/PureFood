package page.codeberg.unix_supremacist.purefood.api.item;

import enviromine.handlers.EM_StatusManager;
import enviromine.trackers.EnviroDataTracker;
import net.minecraftforge.common.util.ForgeDirection;
import page.codeberg.unix_supremacist.purefood.CommonProxy;
import page.codeberg.unix_supremacist.purefood.Tags;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import lombok.Getter;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumAction;
import net.minecraft.item.Item;
import net.minecraft.item.ItemSeedFood;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;

import java.util.HashMap;
import java.util.List;

public class MetaItem extends ItemSeedFood{
	@Getter final String modid;
	public static final HashMap<Integer, MetaItemInstance> items = new HashMap<>();
	@SideOnly(Side.CLIENT)
	IIcon[] icons;

	public MetaItem(String modid, Block farmland) {
		super( 0, 0, null, farmland);
		this.modid = modid;
		this.setHasSubtypes(true);
		this.setCreativeTab(CommonProxy.pftab);
	}

	public ItemStack addItem(String name, int meta, Block crop, int hunger, float saturation, float thirst, boolean isMeat) {
		this.items.put(meta, new MetaItemInstance(name, crop, hunger, saturation, thirst, isMeat));
		return new ItemStack(this, 1, meta);
	}

	protected MetaItemInstance getItem(ItemStack item) {
		return this.items.get(item.getItemDamage());
	}

	/**
	 * Get hunger
	 *
	 * @param item food being checked
	 * @return the hunger restored
	 */
	@Override
	public int func_150905_g(ItemStack item) {
		return this.getItem(item).getHunger();
	}

	/**
	 * Get saturation
	 *
	 * @param item food being checked
	 * @return the saturation float
	 */
	@Override
	public float func_150906_h(ItemStack item) {
		return this.getItem(item).getSaturation();
	}

	public float getThirst(ItemStack item) {
		return this.getItem(item).getThirst();
	}

	@Override
	public String getUnlocalizedName(ItemStack item) {
		if (this.getItem(item) == null) return "item.null";
		return "item."+this.getModid()+"."+this.getItem(item).getName();
	}

	@Override
	public void getSubItems(Item item, CreativeTabs tabs, List itemList) {
		for (Integer id : this.items.keySet()) {
			itemList.add(new ItemStack(this, 1, id));
		}
	}

	@Override
	@SideOnly(Side.CLIENT)
	public void registerIcons(IIconRegister register) {
		for (MetaItemInstance item : this.items.values()) {
			item.setIcon(register.registerIcon(this.getModid()+":"+item.getName()));
		}
	}

	@Override
	@SideOnly(Side.CLIENT)
	public IIcon getIconFromDamage(int meta) {
		MetaItemInstance item = this.items.getOrDefault(meta, null);
		IIcon icon = null;
		if (item != null) {
			icon = item.getIcon();
		}
		return icon;
	}

	@Override
	public boolean onItemUse(ItemStack item, EntityPlayer player, World world, int x, int y, int z, int u, float u1, float u2, float u3) {
		if (this.getItem(item).getCrop() != null) {
			boolean itemUsed = false;
			if (u == 1) {
				if (player.canPlayerEdit(x, y, z, u, item) && player.canPlayerEdit(x, y + 1, z, u, item)) {
					if (world.getBlock(x, y, z).canSustainPlant(world, x, y, z, ForgeDirection.UP, this) && world.isAirBlock(x, y + 1, z)) {
						world.setBlock(x, y + 1, z, this.getItem(item).getCrop());
						item.stackSize--;
						itemUsed = true;
					}
				}
			}
			return itemUsed;
		}
		return false;
	}

	@Override
	public EnumAction getItemUseAction(ItemStack item) {
		if(this.getItem(item).getThirst() > 0)
			return EnumAction.drink;
		else if(this.getItem(item).getHunger() != 0 || this.getItem(item).getSaturation() != 0)
			return EnumAction.eat;
		return EnumAction.none;
	}

	@Override
	public ItemStack onItemRightClick(ItemStack item, World world, EntityPlayer player) {
		if(this.getItem(item).getThirst() != 0 || this.getItem(item).getHunger() != 0 || this.getItem(item).getSaturation() != 0)
			player.setItemInUse(item, this.getMaxItemUseDuration(item));
		return item;
	}

	@Override
	public ItemStack onEaten(ItemStack item, World world, EntityPlayer player){
		if (this.getItem(item).getThirst() != 0) if(Tags.enviromine){
			EnviroDataTracker tracker = EM_StatusManager.lookupTracker(player);

			if(tracker != null){
				if(tracker.bodyTemp > 37.05F) tracker.bodyTemp -= 0.05F;
				tracker.hydrate(this.getItem(item).getThirst());
			}
		}

		return super.onEaten(item, world, player);
	}

	public static boolean isMeat(int meta){
		return items.get(meta).isWolf();
	}
}