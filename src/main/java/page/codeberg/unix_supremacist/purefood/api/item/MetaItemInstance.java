package page.codeberg.unix_supremacist.purefood.api.item;

import lombok.Getter;
import lombok.Setter;
import net.minecraft.block.Block;
import net.minecraft.util.IIcon;

public class MetaItemInstance {
	@Getter final String name;
	@Getter @Setter
	IIcon icon;
	@Getter final int hunger;
	@Getter final float saturation;
	@Getter final float thirst;
	@Getter final Block crop;
	@Getter final boolean wolf;

	public MetaItemInstance(String name, Block crop, int healing, float saturation, float thirst, boolean wolf) {
		this.name = name;
		this.hunger = healing;
		this.saturation = saturation;
		this.thirst = thirst;
		this.crop = crop;
		this.wolf = wolf;
	}
}