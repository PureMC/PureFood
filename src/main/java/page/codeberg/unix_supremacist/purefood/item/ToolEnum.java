package page.codeberg.unix_supremacist.purefood.item;

import net.minecraft.item.Item;
import page.codeberg.unix_supremacist.purefood.Tags;
import page.codeberg.unix_supremacist.purefood.api.item.ToolItem;

public enum ToolEnum {
    MORTAR_FLINT(256),
    MORTAR_COPPER(384),
    MORTAR_IRON(512),
    MORTAR_THAUMIUM(640),
    MORTAR_STEEL(768),
    MORTAR_OSMIUM(768),
    MORTAR_DIAMOND(1024),
    MORTAR_DARK_STEEL(1024),
    MORTAR_MANA_STEEL(1024),
    MORTAR_NETHERITE(1536),
    MORTAR_TUNGSTENSTEEL(1536),
    MORTAR_DRACONIUM(1536),
    KNIFE_FLINT(256),
    KNIFE_COPPER(384),
    KNIFE_IRON(512),
    KNIFE_THAUMIUM(640),
    KNIFE_STEEL(768),
    KNIFE_OSMIUM(768),
    KNIFE_DIAMOND(1024),
    KNIFE_DARK_STEEL(1024),
    KNIFE_MANA_STEEL(1024),
    KNIFE_NETHERITE(1536),
    KNIFE_TUNGSTENSTEEL(1536),
    KNIFE_DRACONIUM(1536);

    public Item item;
    ToolEnum(int dura) {
        this.item = new ToolItem(this.name(), Tags.MODID, dura).setUnlocalizedName(this.name().toLowerCase());
    }
}
