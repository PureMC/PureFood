package page.codeberg.unix_supremacist.purefood.item;

import page.codeberg.unix_supremacist.purefood.config.DynamicConfigs;
import page.codeberg.unix_supremacist.purefood.Tags;
import page.codeberg.unix_supremacist.purefood.api.block.CropBase;
import page.codeberg.unix_supremacist.purefood.api.item.MetaItem;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;

public class ItemIterator {
    public static MetaItem metaItem;
    public static void load() {
        //Create items
        metaItem = new MetaItem(Tags.MODID, Blocks.farmland);
        //Registers them to the forge registry
        GameRegistry.registerItem(metaItem, "metaItem");
        //Iterates items
        for (ConsumableEnum item : ConsumableEnum.values()) {
            if(DynamicConfigs.getToggled(item.name(), true)) addItem(
                    Tags.MODID, metaItem, item.name().toLowerCase(), item.ordinal(),
                    DynamicConfigs.getHunger(item.name(), item.hunger),
                    DynamicConfigs.getSaturation(item.name(), item.saturation),
                    item.thirst, item.crop, item.meat
            );
        }

        for (ToolEnum tool : ToolEnum.values()) {
            if(tool.item != null) GameRegistry.registerItem(tool.item, tool.name().toLowerCase());
        }
    }

    public static ItemStack addItem(String MODID, MetaItem item, String name, int meta, int hunger, float saturation, float thirst, boolean hasCrop, boolean isMeat){
        if (hasCrop){
            CropBase crop = new CropBase(MODID, name, item, meta);
            GameRegistry.registerBlock(crop, name);
            return item.addItem(name, meta, crop, hunger, saturation, thirst, isMeat);
        } else {
            return item.addItem(name, meta, null, hunger, saturation, thirst, isMeat);
        }
    }
}