package page.codeberg.unix_supremacist.purefood.item;

public enum ConsumableEnum {
    AMBROSIA(0f),
    ANANAS(4, 0.3f),
    APPLE_CORE(0, 0.0f),
    BACON_AND_EGGS(0, 0.0f), //todo
    BAGUETTE( 8, 1.2f),
    BAGUETTES( 8, 1.2f),
    BANANA( 1, 0.6f),
    BAR_BUTTER( 1, 4.0f),
    BAR_CHEESE( 2, 1.2f),
    BAR_CHOCOLATE( 4, 0.8f),
    BAR_SOYLENT(0, 0.0f), //todo
    BAR_TOFU(0, 0.0f), //todo
    BARK_CINNAMON(0, 0.0f), //todo
    BAWLS(0f),
    BBQ_RIBS(10, 1.6f, true),
    BEANS(0, 0.0f),
    BEANS_MUNG(0, 0.0f), //todo
    BEANS_RED(0, 0.0f), //todo
    BEANS_URAD(0, 0.0f), //todo
    BEET(0, 0.0f),
    BERRY_BILL(1, 0.6f),
    BERRY_BLACK(1, 0.6f),
    BERRY_BLUE(1, 0.6f),
    BERRY_CANDLE(1, 0.6f),
    BERRY_CRAN(1, 0.6f),
    BERRY_FISHER(1, 0.6f),
    BERRY_GOOSE(1, 0.6f),
    BERRY_MUL(1, 0.6f),
    BERRY_PRICKLE(1, 0.6f),
    BERRY_RASP(1, 0.6f),
    BERRY_STRAW(1, 0.6f),
    BERRY_SUN(1, 0.6f),
    BIG_BREAD(0, 0.0f), //todo
    BOILED_EGG(2, 1.2f),
    BREADS(5, 1.2f),
    BROTH_BONE(0, 0.0f), //todo
    BRYONY(0, 0.0f), //todo
    BUN(2, 1.2f),
    BUNS(2, 1.2f),
    BURGER(6, 1.6f, true),
    BURGER_CHEESE(4, 1.4f, true),
    BURGER_CHUM(6, 1.6f, true),
    BURGER_FISH(6, 1.6f, true),
    BURGER_MEAT(0, 0.0f, true), //todo
    BURGER_SOYLENT(0, 0.0f), //todo
    BURGER_TOFU(0, 0.0f), //todo
    BURGER_VEGGIE(0, 0.0f), //todo
    CABBAGE(0, 0.0f), //todo
    CAKE_BOTTOM(3, 0.2f),
    CAKE_CHEESE_SWEET_BERRY(), //todo
    CHEESE(0, 0.0f), //todo
    CHEESE_AGED(0, 0.0f), //todo
    CHICKPEAS(0, 0.0f), //todo
    CHIPS_CHILI(0, 0.0f), //todo
    CHIPS(0, 0.0f), //todo
    CHUM(0, 0.0f, true), //todo
    CIDER(0f),
    CIDER_ANANAS(0f),
    CIDER_APPLE(0f),
    COCONUT(0, 0.0f), //todo
    COOKED_BACON(0, 0.0f, true), //todo
    COOKED_CHEVAL(0, 0.0f, true), //todo
    COOKED_CHICKEN_WINGS(0, 0.0f, true), //todo
    COOKED_CHOPS_MUTTON(0, 0.0f, true), //todo
    COOKED_DOGMEAT(0, 0.0f, true), //todo
    COOKED_HAM(0, 0.0f, true), //todo
    COOKED_HORSE_MEAT(0, 0.0f, true), //todo
    COOKED_MUTTON(7, 2.0f, true),
    COOKED_MYSTERYMEAT(0, 0.0f, true), //todo
    COOKED_PATTY_BEEF(0, 0.0f, true), //todo
    COOKED_RIB_EYE_STEAK(0, 0.0f, true), //todo
    COOKED_RIBS(0, 0.0f, true), //todo
    COOKED_RICE(0, 0.0f, true), //todo
    COOKED_SAUSAGE(0, 0.0f, true), //todo
    COOKED_SLAB_BEEF(0, 0.0f, true), //todo
    COOKED_SLAB_CHEVAL(0, 0.0f, true), //todo
    COOKED_SLAB_MOOSHROOM(0, 0.0f, true), //todo
    COOKED_SLAB_PORK(0, 0.0f, true), //todo
    COOKED_SLICE_HAM(0, 0.0f, true), //todo
    COOKIE_CHOCOLATE_RAISIN(0, 0.0f), //todo
    COOKiE_RAISIN(0, 0.0f), //todo
    COOKIE_SWEET_BERRY(0, 0.0f), //todo
    CORNKERNELS(0, 0.0f), //todo
    CREAM_CHOCOLATE(0, 0.0f), //todo
    CREAM_COCONUT(0, 0.0f), //todo
    CREAM_HEAVY(0, 0.0f), //todo
    CUCUMBER(0, 0.0f), //todo
    CURDS_CHEESE(0, 0.0f), //todo
    CURRANTS_BLACK(0, 0.0f), //todo
    CURRANTS_RED(0, 0.0f), //todo
    CURRANTS_WHITE(0, 0.0f), //todo
    DOG_FOOD(0, 0.0f, true), //todo
    DOGMEAT(0, 0.0f, true), //todo
    DOUGH(0, 0.0f), //todo
    DOUGH_ABYSSAL(1, 1.0f),
    DOUGH_CHOCOLATE(0, 0.0f), //todo
    DOUGH_COOKIE(0, 0.0f), //todo
    DOUGH_COOKIE_ABYSSAL( 1, 0.2f),
    DOUGH_COOKIE_CHOCOLATE_RAISIN(0, 0.0f), //todo
    DOUGH_COOKIE_RAISIN(0, 0.0f), //todo
    DOUGH_LOAF(0, 0.0f), //todo
    DOUGH_PASTA(0, 0.0f), //todo
    DOUGH_PASTA_FLAT(0, 0.0f), //todo
    DOUGH_PIZZA_TOMATO_SAUCED(0, 0.0f), //todo
    DOUGH_SUGAR(0, 0.0f), //todo
    DOUGH_SUGAR_CHOCOLATE_RAISIN(0, 0.0f), //todo
    DOUGH_SUGAR_RAISIN(0, 0.0f), //todo
    DRESSING(0, 0.0f), //todo
    DRIED_JERKY(0, 0.0f, true), //todo
    DRIED_RAISINS_CHOCOLATE(0, 0.0f), //todo
    DRIED_RAISINS_GREEN(0, 0.0f), //todo
    DRIED_RAISINS_POMEGRANATE(0, 0.0f), //todo
    DRIED_RAISINS_PURPLE(0, 0.0f), //todo
    DRIED_RAISINS_RED(0, 0.0f), //todo
    DRIED_RAISINS_WHITE(0, 0.0f), //todo
    DUMPLINGS(0, 0.0f), //todo
    EGG_SCRAMBLED(0, 0.0f), //todo
    EGG_WHITE(0, 0.0f), //todo
    EGG_YOLK(0, 0.0f), //todo
    FIG(0, 0.0f), //todo
    FRIED_EGG(0, 0.0f), //todo
    FRIED_RICE(0, 0.0f), //todo
    FRIES(0, 0.0f), //todo
    GARLIC(0, 0.0f), //todo
    GLOW_BERRY_CUSTARD(0, 0.0f), //todo
    GRAIN_BARLEY(),
    GRAIN_RICE(),
    GRAIN_RYE(),
    GRAPES_GREEN(0, 0.0f), //todo
    GRAPES_PURPLE(0, 0.0f), //todo
    GRAPES_RED(0, 0.0f), //todo
    GRAPES_WHITE(0, 0.0f), //todo
    GRILLED_SALMON(0, 0.0f, true), //todo
    HARDTACK(0, 0.0f), //todo
    HAZELNUT(0, 0.0f), //todo
    HONEY(0, 0.0f), //todo
    HONEY_GLAZED_HAM(0, 0.0f, true), //todo
    HONEYDEW(0, 0.0f), //todo
    HORSE_FEED(0, 0.0f),
    HOT_COCOA(0f),
    ICE_CREAM(0, 0.0f), //todo
    ICE_CREAM_ANANAS(0, 0.0f), //todo
    ICE_CREAM_APPLE(0, 0.0f), //todo
    ICE_CREAM_BACON(0, 0.0f), //todo
    ICE_CREAM_BANANA(0, 0.0f), //todo
    ICE_CREAM_BERRY_BLACK(0, 0.0f), //todo
    ICE_CREAM_BERRY_BLUE(0, 0.0f), //todo
    ICE_CREAM_BERRY_CRAN(0, 0.0f), //todo
    ICE_CREAM_BERRY_GOOSE(0, 0.0f), //todo
    ICE_CREAM_BERRY_RASP(0, 0.0f), //todo
    ICE_CREAM_BERRY_STRAW(0, 0.0f), //todo
    ICE_CREAM_CARAMEL(0, 0.0f), //todo
    ICE_CREAM_CHERRY(0, 0.0f), //todo
    ICE_CREAM_CHOCOLATE_CHIP(0, 0.0f), //todo
    ICE_CREAM_CHOCOLATE(0, 0.0f), //todo
    ICE_CREAM_CHUM(0, 0.0f), //todo
    ICE_CREAM_CURRANT(0, 0.0f), //todo
    ICE_CREAM_GRAPE(0, 0.0f), //todo
    ICE_CREAM_HONEY(0, 0.0f), //todo
    ICE_CREAM_KIWI(0, 0.0f), //todo
    ICE_CREAM_LEMON(0, 0.0f), //todo
    ICE_CREAM_MAPLE(0, 0.0f), //todo
    ICE_CREAM_MELON(0, 0.0f), //todo
    ICE_CREAM_MINT_CHOCOLATE_CHIP(0, 0.0f), //todo
    ICE_CREAM_MINT(0, 0.0f), //todo
    ICE_CREAM_MOCHA(0, 0.0f), //todo
    ICE_CREAM_NEAPOLITAN(0, 0.0f), //todo
    ICE_CREAM_NUTELLA(0, 0.0f), //todo
    ICE_CREAM_PEANUT_BUTTER(0, 0.0f), //todo
    ICE_CREAM_PISTACHIO(0, 0.0f), //todo
    ICE_CREAM_RAISIN(0, 0.0f), //todo
    ICE_CREAM_SPUMONI_CHOCOLATE(0, 0.0f), //todo
    ICE_CREAM_SPUMONI_VANILLA(0, 0.0f), //todo
    ICE_CREAM_VANILLA(0, 0.0f), //todo
    JUICE_ANANAS(10f), //todo
    JUICE_APPLE(0f),
    JUICE_APRICOT(0f),
    JUICE_BANANA(0f),
    JUICE_BEET(0f),
    JUICE_BERRY_BLACK(0f),
    JUICE_BERRY_BLUE(0f),
    JUICE_BERRY_CRAN(0f),
    JUICE_BERRY_ELDER(0f),
    JUICE_BERRY_GOOSE(0f),
    JUICE_BERRY_RASP(0f),
    JUICE_BERRY_STRAW(0f),
    JUICE_CARROT(0f),
    JUICE_CHERRY(0f),
    JUICE_COCONUT(0f),
    JUICE_FIG(0f),
    JUICE_FRUIT(0f),
    JUICE_GRAPE(0f),
    JUICE_GRAPEFRUIT(0f),
    JUICE_KIWI(0f),
    JUICE_LEMON(0f),
    JUICE_LIME(0f),
    JUICE_MANGO(0f),
    JUICE_MELON(0f),
    JUICE_ORANGE(0f),
    JUICE_PAPAYA(0f),
    JUICE_PEACH(0f),
    JUICE_PEAR(0f),
    JUICE_PERSIMMON(0f),
    JUICE_PLUM(0f),
    JUICE_POMEGRANATE(0f),
    JUICE_POTATO(0f),
    JUICE_PUMPKIN(0f),
    JUICE_PURPLE(0f),
    JUICE_STARFRUIT(0f),
    JUICE_TOMATO(0f),
    LEAF_CABBAGE(0, 0.0f), //todo
    LEEK(0, 0.0f), //todo
    LEMON(0, 0.0f), //todo
    LEMONADE(10f), //todo
    LENTILS(0, 0.0f), //todo
    LETTUCE(0, 0.0f), //todo
    LIMONCELLO(0, 0.0f), //todo
    LOAF(0, 0.0f), //todo
    MASH_HOPS(0f), //todo
    MASH_WHEAT_HOPS(0f), //todo
    MASH_WHEAT(0f), //todo
    MILK(10f), //todo
    MILK_COCONUT(10f), //todo
    MILK_SOY(10f), //todo
    MUCKROOT(0, 0.0f), //todo
    MUSHROOM_RICE(0, 0.0f), //todo
    NOODLES_VEGETABLE(0, 0.0f), //todo
    OATS(0, 0.0f), //todo
    OIL_FISH(), //todo
    OIL_HEMP(), //todo
    OIL_LIN(), //todo
    OIL_NUT(), //todo
    OIL_OLIVE(), //todo
    OIL_SEED(), //todo
    OIL_SUNFLOWER(), //todo
    OIL_WHALE(), //todo
    PANTAO(0, 0.0f), //todo
    PASTA_MEATBALLS(0, 0.0f, true), //todo
    PASTA_MUTTON_CHOP(0, 0.0f, true), //todo
    PASTA_SQUID_INK(0, 0.0f), //todo
    PEANUT(0, 0.0f), //todo
    PEPPER(0, 0.0f), //todo
    PIE_APPLE(0, 0.0f), //todo
    PIE_CHOCOLATE(0, 0.0f), //todo
    PIE_CRUST(0, 0.0f), //todo
    PIE_SHEPHERDS(0, 0.0f), //todo
    PILE_RICE(0, 0.0f), //todo
    PIZZA_DOUGH(0, 0.0f), //todo
    PIZZA_MARGHERITA(0, 0.0f), //todo
    PIZZA_MINCE_MEAT(0, 0.0f), //todo
    PIZZA_PINEAPPLE_HAM(0, 0.0f), //todo
    PIZZA_VEGGIE(0, 0.0f), //todo
    POMEGRANATE(0, 0.0f), //todo
    RADISH(0, 0.0f), //todo
    RATATOUILLE(0, 0.0f), //todo
    RAW_BACON(0, 0.0f, true), //todo
    RAW_BAGUETTE(0, 0.0f), //todo
    RAW_BREAD(0, 0.0f), //todo
    RAW_BUN(0, 0.0f), //todo
    RAW_CAKE_BOTTOM(0, 0.0f), //todo
    RAW_CHEVAL(0, 0.0f, true), //todo
    RAW_CHICKEN_WINGS(0, 0.0f, true), //todo
    RAW_CHIPS(0, 0.0f), //todo
    RAW_CHOPS_MUTTON(0, 0.0f, true), //todo
    RAW_HAM(0, 0.0f, true), //todo
    RAW_HORSE_MEAT(0, 0.0f, true), //todo
    RAW_MARGHERITA_PIZZA(0, 0.0f), //todo
    RAW_MINCE_MEAT_PIZZA(0, 0.0f, true), //todo
    RAW_MUTTON(0, 0.0f, true), //todo
    RAW_MYSTERYMEAT(0, 0.0f, true), //todo
    RAW_PASTA(0, 0.0f), //todo
    RAW_PATTY_BEEF(0, 0.0f, true), //todo
    RAW_PINEAPPLE_HAM_PIZZA(0, 0.0f, true), //todo
    RAW_RIB_EYE_STEAK(0, 0.0f, true), //todo
    RAW_RIBS(0, 0.0f, true), //todo
    RAW_SAUSAGE(0, 0.0f, true), //todo
    RAW_SLAB_BEEF(0, 0.0f, true), //todo
    RAW_SLAB_CHEVAL(0, 0.0f, true), //todo
    RAW_SLAB_MOOSHROOM(0, 0.0f, true), //todo
    RAW_SLAB_PORK(0, 0.0f, true), //todo
    RAW_VEGGIE_PIZZA(0, 0.0f), //todo
    RED_ONION(0, 0.0f), //todo
    ROAST_CHICKEN(0, 0.0f, true), //todo
    ROAST_CHOPS_MUTTON(0, 0.0f, true), //todo
    ROLL_CABBAGE(0, 0.0f), //todo
    ROLL_COD(0, 0.0f, true), //todo
    ROLL_KELP(0, 0.0f), //todo
    ROLL_SALMON(0, 0.0f, true), //todo
    ROTTEN_TOMATO(0, 0.0f), //todo
    SALAD_FRUIT(0, 0.0f), //todo
    SALAD_MIXED(0, 0.0f), //todo
    SALAD_NETHER(0, 0.0f), //todo
    SALTED_BAR_BUTTER(0, 0.0f), //todo
    SALTED_SLAB_BEEF(0, 0.0f, true), //todo
    SALTED_SLAB_CHEVAL(0, 0.0f, true), //todo
    SALTED_SLAB_PORK(0, 0.0f, true), //todo
    SANDWICH_BACON(0, 0.0f, true), //todo
    SANDWICH_CHEESE(0, 0.0f), //todo
    SANDWICH_CHICKEN(0, 0.0f, true), //todo
    SANDWICH_EGG(0, 0.0f), //todo
    SANDWICH_LARGE_BACON(0, 0.0f, true), //todo
    SANDWICH_LARGE_CHEESE(0, 0.0f), //todo
    SANDWICH_LARGE_STEAK(0, 0.0f, true), //todo
    SANDWICH_LARGE_VEGGIE(0, 0.0f), //todo
    SANDWICH_STEAK(0, 0.0f, true), //todo
    SANDWICH_VEGGIE(0, 0.0f), //todo
    SAUCE_BBQ(), //todo
    SAUCE_CHILI(), //todo
    SAUCE_HOT(), //todo
    SAUCE_KETCHUP(), //todo
    SAUCE_MAPLE_SAP(), //todo
    SAUCE_MAPLE_SYRUP(), //todo
    SAUCE_MAYO(), //todo
    SAUCE_PEANUT_BUTTER(), //todo
    SAUCE_ROYAL_JELLY(), //todo
    SAUCE_TOMATO(), //todo
    SCRAP_MEAT(0, 0.0f, true), //todo
    SILVER_PEAR(0, 0.0f), //todo
    SLICE_ANANAS(0, 0.0f), //todo
    SLICE_APPLE(0, 0.0f), //todo
    SLICE_BAGUETTE(0, 0.0f), //todo
    SLICE_BANANA(0, 0.0f), //todo
    SLICE_BREAD(0, 0.0f), //todo
    SLICE_BUN(0, 0.0f), //todo
    SLICE_CAKE(0, 0.0f), //todo
    SLICE_CAKE_CHEESE_SWEET_BERRY(0, 0.0f), //todo
    SLICE_CANTALOUPE(0, 0.0f), //todo
    SLICE_CARROT(0, 0.0f), //todo
    SLICE_CHEESE(0, 0.0f), //todo
    SLICE_COCONUT(0, 0.0f), //todo
    SLICE_COOKED_COD(0, 0.0f, true), //todo
    SLICE_COOKED_FISH(0, 0.0f, true), //todo
    SLICE_COOKED_SALMON(0, 0.0f, true), //todo
    SLICE_CUCUMBER(0, 0.0f), //todo
    SLICE_EGG(0, 0.0f), //todo
    SLICE_EGGPLANT(0, 0.0f), //todo
    SLICE_HONEYDEW(0, 0.0f), //todo
    SLICE_HORNEDMELON(0, 0.0f), //todo
    SLICE_LEMON(0, 0.0f), //todo
    SLICE_LOAF(0, 0.0f), //todo
    SLICE_ONION(0, 0.0f), //todo
    SLICE_PIE_APPLE(0, 0.0f), //todo
    SLICE_PIE_CHOCOLATE(0, 0.0f), //todo
    SLICE_PIE_PUMPKIN(0, 0.0f), //todo
    SLICE_PUMPKIN(0, 0.0f), //todo
    SLICE_RAW_COD(0, 0.0f, true), //todo
    SLICE_RAW_FISH(0, 0.0f, true), //todo
    SLICE_RAW_HAM(0, 0.0f, true), //todo
    SLICE_RAW_SALMON(0, 0.0f, true), //todo
    SLICE_ROLL_KELP(0, 0.0f), //todo
    SLICE_SMOKED_FISH(0, 0.0f, true), //todo
    SLICE_SMOKED_SALMON(0, 0.0f, true), //todo
    SLICE_SQUASH(0, 0.0f), //todo
    SLICE_TOMATO(0, 0.0f), //todo
    SLICE_WINTERMELON(0, 0.0f), //todo
    SLIME_BOTTLE_BLUE(), //todo
    SLIME_BOTTLE_GREEN(), //todo
    SLIME_BOTTLE_PINK(), //todo
    SMOOTHIE_ANANAS(0f), //todo
    SMOOTHIE_APPLE(0f), //todo
    SMOOTHIE_APRICOT(0f), //todo
    SMOOTHIE_BANANA(0f), //todo
    SMOOTHIE_BERRY_BLACK(0f), //todo
    SMOOTHIE_BERRY_BLUE(0f), //todo
    SMOOTHIE_BERRY_CRAN(0f), //todo
    SMOOTHIE_BERRY_ELDER(0f), //todo
    SMOOTHIE_BERRY_GOOSE(0f), //todo
    SMOOTHIE_BERRY_RASP(0f), //todo
    SMOOTHIE_BERRY_STRAW(0f), //todo
    SMOOTHIE_CHERRY(0f), //todo
    SMOOTHIE_COCONUT(0f), //todo
    SMOOTHIE_CURRANT(0f), //todo
    SMOOTHIE_FIG(0f), //todo
    SMOOTHIE_FRUIT(0f), //todo
    SMOOTHIE_GRAPE(0f), //todo
    SMOOTHIE_GRAPEFRUIT(0f), //todo
    SMOOTHIE_KIWI(0f), //todo
    SMOOTHIE_LEMON(0f), //todo
    SMOOTHIE_LIME(0f), //todo
    SMOOTHIE_MANGO(0f), //todo
    SMOOTHIE_MELON(0f), //todo
    SMOOTHIE_ORANGE(0f), //todo
    SMOOTHIE_PAPAYA(0f), //todo
    SMOOTHIE_PEACH(0f), //todo
    SMOOTHIE_PEAR(0f), //todo
    SMOOTHIE_PERSIMMON(0f), //todo
    SMOOTHIE_PLUM(0f), //todo
    SMOOTHIE_POMEGRANATE(0f), //todo
    SMOOTHIE_STARFRUIT(0f), //todo
    SOUP_CHICKEN(0, 0.0f), //todo
    SOUP_NOODLE(0, 0.0f), //todo
    SOUP_PUMPKIN(0, 0.0f), //todo
    SOUP_VEGETABLE(0, 0.0f), //todo
    SPICES(0, 0.0f), //todo
    SPINACH(0, 0.0f), //todo
    STEAK_AND_POTATOES(0, 0.0f), //todo
    STEW_BAKED_COD(0, 0.0f), //todo
    STEW_BEEF(0, 0.0f), //todo
    STEW_FISH(0, 0.0f), //todo
    STEW_SWEETPOD(0, 0.0f), //todo
    STRIPS_POTATO(0, 0.0f), //todo
    STUFFED_POTATO(0, 0.0f), //todo
    STUFFED_PUMPKIN(0, 0.0f), //todo
    SWEETPOD(0, 0.0f), //todo
    TOAST(0, 0.0f), //todo
    TOMATILLOS(0, 0.0f), //todo
    TOMATO(0, 0.0f), //todo
    TURKISH_DELIGHT(0, 0.0f), //todo
    VINEGAR_CANE(0, 0.0f), //todo
    VINEGAR_CIDER(0, 0.0f), //todo
    VINEGAR_GRAPE(0, 0.0f), //todo
    VINEGAR_RICE(0, 0.0f), //todo
    WATER_CACTUS(0f), //todo
    WATER_CARBONATED_MINERAL(0f), //todo
    WATER_CARBONATED(0f), //todo
    WATER_COLD(0f), //todo
    WATER_HOLY(0f), //todo
    WATER_IMPURE(0f), //todo
    WATER_MINERAL(0f), //todo
    WATER_REED(0f), //todo
    WATER_RICE(0f), //todo
    WATER_SEA(0f), //todo
    WHEEL_AGED_CHEESE(0, 0.0f), //todo
    WHEEL_CHEESE(0, 0.0f), //todo
    WHITE_ONION(0, 0.0f), //todo
    WHEEL_WAXED_CHEESE(0, 0.0f), //todo
    WISDOMFRUIT(0, 0.0f), //todo
    WRAP_MUTTON(0, 0.0f, true); //todo

    boolean crop;
    int hunger;
    float saturation;
    float thirst;
    boolean meat;

    ConsumableEnum(boolean crop, int hunger, float saturation, float thirst, boolean meat) {
        this.crop = crop;
        this.hunger = hunger;
        this.saturation = saturation;
        this.thirst = 0;
        this.meat = meat;
    }

    ConsumableEnum(boolean crop, int hunger, float saturation) {
        this(crop, hunger, saturation, 0, false);
    }

    ConsumableEnum(int hunger, float saturation, float thirst) {
        this(false, hunger, saturation, thirst, false);
    }

    ConsumableEnum(int hunger, float saturation) {
        this(false, hunger, saturation, 0, false);
    }

    ConsumableEnum(int hunger, float saturation, boolean meat) {
        this(false, hunger, saturation, 0, meat);
    }

    ConsumableEnum(Float thirst) {
        this(false, 0, 0, thirst, false);
    }

    ConsumableEnum() {
        this(false, 0, 0, 0, false);
    }
}