package page.codeberg.unix_supremacist.purefood.recipe;

import net.minecraft.item.Item;
import page.codeberg.unix_supremacist.purefood.item.ConsumableEnum;
import page.codeberg.unix_supremacist.purefood.item.ItemIterator;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;
import page.codeberg.unix_supremacist.purefood.item.ToolEnum;

public class Oredict {
    public static String mortarOD="toolMortarandpestle";
    public static String knifeOD="toolKnife";

    public static void load(){
        loadTools();
        loadFood();
    }

    public static void loadFood(){
        itemRegister("listAllfruit", ConsumableEnum.ANANAS.ordinal());
        itemRegister("itemPlantRemains", ConsumableEnum.APPLE_CORE.ordinal());
        itemRegister("listAllfruit", ConsumableEnum.BANANA.ordinal());
        itemRegister("foodButter", ConsumableEnum.BAR_BUTTER.ordinal());
        itemRegister("foodCheese", ConsumableEnum.BAR_CHEESE.ordinal());
        itemRegister("foodChocolatebar", ConsumableEnum.BAR_CHOCOLATE.ordinal());
        itemRegister("listAllmeatraw", ConsumableEnum.BAR_SOYLENT.ordinal());
        itemRegister("listAllmeatraw", ConsumableEnum.BAR_TOFU.ordinal());
        itemRegister("foodRibbbq", ConsumableEnum.BBQ_RIBS.ordinal());
        itemRegister("listAllveggie", ConsumableEnum.BEANS.ordinal());
        itemRegister("listAllveggie", ConsumableEnum.BEANS_MUNG.ordinal());
        itemRegister("listAllveggie", ConsumableEnum.BEANS_RED.ordinal());
        itemRegister("listAllveggie", ConsumableEnum.BEANS_URAD.ordinal());
    }

    public static void loadTools(){
        toolRegister(mortarOD, ToolEnum.MORTAR_FLINT.item);
        toolRegister(mortarOD, ToolEnum.MORTAR_COPPER.item);
        toolRegister(mortarOD, ToolEnum.MORTAR_IRON.item);
        toolRegister(mortarOD, ToolEnum.MORTAR_THAUMIUM.item);
        toolRegister(mortarOD, ToolEnum.MORTAR_STEEL.item);
        toolRegister(mortarOD, ToolEnum.MORTAR_OSMIUM.item);
        toolRegister(mortarOD, ToolEnum.MORTAR_DIAMOND.item);
        toolRegister(mortarOD, ToolEnum.MORTAR_DARK_STEEL.item);
        toolRegister(mortarOD, ToolEnum.MORTAR_MANA_STEEL.item);
        toolRegister(mortarOD, ToolEnum.MORTAR_NETHERITE.item);
        toolRegister(mortarOD, ToolEnum.MORTAR_TUNGSTENSTEEL.item);
        toolRegister(mortarOD, ToolEnum.MORTAR_DRACONIUM.item);
        toolRegister(knifeOD, ToolEnum.KNIFE_FLINT.item);
        toolRegister(knifeOD, ToolEnum.KNIFE_COPPER.item);
        toolRegister(knifeOD, ToolEnum.KNIFE_IRON.item);
        toolRegister(knifeOD, ToolEnum.KNIFE_THAUMIUM.item);
        toolRegister(knifeOD, ToolEnum.KNIFE_STEEL.item);
        toolRegister(knifeOD, ToolEnum.KNIFE_OSMIUM.item);
        toolRegister(knifeOD, ToolEnum.KNIFE_DIAMOND.item);
        toolRegister(knifeOD, ToolEnum.KNIFE_DARK_STEEL.item);
        toolRegister(knifeOD, ToolEnum.KNIFE_MANA_STEEL.item);
        toolRegister(knifeOD, ToolEnum.KNIFE_NETHERITE.item);
        toolRegister(knifeOD, ToolEnum.KNIFE_TUNGSTENSTEEL.item);
        toolRegister(knifeOD, ToolEnum.KNIFE_DRACONIUM.item);
    }

    public static void itemRegister(String od, int meta){
        if(new ItemStack(ItemIterator.metaItem, 1, meta).getUnlocalizedName() != "item.null"){
            OreDictionary.registerOre(od, new ItemStack(ItemIterator.metaItem, 1, meta));
        }
    }

    public static void toolRegister(String od, Item item){
        if(item != null) OreDictionary.registerOre(od, new ItemStack(item, 1, OreDictionary.WILDCARD_VALUE));
    }
}