package page.codeberg.unix_supremacist.purefood.recipe;

import page.codeberg.unix_supremacist.purefood.item.ConsumableEnum;
import page.codeberg.unix_supremacist.purefood.item.ItemIterator;
import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.ShapelessOreRecipe;

public class Recipe {
    public static void load(){
        GameRegistry.addRecipe(new ShapelessOreRecipe(new ItemStack(ItemIterator.metaItem, 1, ConsumableEnum.DOUGH.ordinal()), new Object[]{net.minecraft.init.Items.wheat, Oredict.mortarOD}));
    }
}
