#PureFood
a lot of textures are taken from gt6 and licensed under CC0

some textures are taken from https://github.com/malcolmriley/unused-textures and licensed under Attribution 4.0 International (CC BY 4.0) (license located under LICENSE-malcolmriley-unused-textures)

#Todo and Ideas
make tomatos more like the red of farmers delight
GT6 like knives for slicing Foods
maybe a FD like cutting board that goes with the GT6 knife for cheaper recipes or easier mass crafting
maybe TaC/FD like heater/skillet (electric heater for mod compat, and make it compatible with GT6/IC2EXP/Mekanism heat)
grill that does smoked and BBQ foods (electric grill for mod compat)
pot block that is a specialized crafting table
juicer block (requires crank, ideally have an electric version for mod compat)
something similar to drying racks from tinkers, that makes sense to use for jerky and also raisins
churn for making butter
way to collect salt from ocean biomes

##Foods that would be cool to be added but needs textures
marshmellows, including on a stick
maybe golden carrot slices
chicken breasts, legs

##Mod compat
gt4/5/6 mixers should contain certain recipes
if nutrition is ported all foods should have values
pams/fossil arch/mekanism etc oredict compat
some sort of compat with EFR mutton
recipes in EFR campfire/smoker